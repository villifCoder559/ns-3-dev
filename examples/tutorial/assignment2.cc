#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/core-module.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/non-communicating-net-device.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/single-model-spectrum-channel.h"
#include "ns3/spectrum-channel.h"
#include "ns3/spectrum-helper.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/system-wall-clock-ms.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/waveform-generator-helper.h"
#include "ns3/waveform-generator.h"
#include "ns3/wifi-net-device.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include <chrono>
#include <iomanip>
#include <stdio.h>
#include <time.h>

using namespace ns3;
double g_signalDbmAvg1; //!< Average signal power [dBm]
double g_noiseDbmAvg1;  //!< Average noise power [dBm]
uint32_t g_samples1;    //!< Number of samples
double g_signalDbmAvg2; //!< Average signal power [dBm]
double g_noiseDbmAvg2;  //!< Average noise power [dBm]
uint32_t g_samples2;    //!< Number of samples

uint32_t payloadSize = 1024;
const uint32_t tcpPacketSize = 1024;
double datarate = 52;
bool udp = true;
double simulationTime = 5; // seconds

NS_LOG_COMPONENT_DEFINE("ThirdScript");

void
MonitorSniffRx1(Ptr<const Packet> packet,
                uint16_t channelFreqMhz,
                WifiTxVector txVector,
                MpduInfo aMpdu,
                SignalNoiseDbm signalNoise,
                uint16_t staId)

{
    g_samples1++;
    g_signalDbmAvg1 += ((signalNoise.signal - g_signalDbmAvg1) / g_samples1);
    g_noiseDbmAvg1 += ((signalNoise.noise - g_noiseDbmAvg1) / g_samples1);
}

void
MonitorSniffRx2(Ptr<const Packet> packet,
                uint16_t channelFreqMhz,
                WifiTxVector txVector,
                MpduInfo aMpdu,
                SignalNoiseDbm signalNoise,
                uint16_t staId)

{
    g_samples2++;
    g_signalDbmAvg2 += ((signalNoise.signal - g_signalDbmAvg2) / g_samples2);
    g_noiseDbmAvg2 += ((signalNoise.noise - g_noiseDbmAvg2) / g_samples2);
}

enum networkType
{
    yans,
    multiModel
};

void
resetGlobalValue()
{
    g_noiseDbmAvg1 = 0;
    g_signalDbmAvg1 = 0;
    g_samples1 = 0;
    g_noiseDbmAvg2 = 0;
    g_samples2 = 0;
}

void
retriveAndWriteData(ApplicationContainer& serverApp,
                    int network_id,
                    Ptr<OutputStreamWrapper> fileStream,
                    double distanceNET1toNET2)
{
    double throughput = 0;
    uint64_t totalPacketsThrough = 0;
    if (udp)
    {
        // UDP
        totalPacketsThrough = DynamicCast<UdpServer>(serverApp.Get(0))->GetReceived();
        throughput = totalPacketsThrough * payloadSize * 8 / (simulationTime * 1000000.0); // Mbit/s
    }
    else
    {
        // TCP
        uint64_t totalBytesRx = DynamicCast<PacketSink>(serverApp.Get(0))->GetTotalRx();
        totalPacketsThrough = totalBytesRx / tcpPacketSize;
        throughput = totalBytesRx * 8 / (simulationTime * 1000000.0); // Mbit/s
    }
    std::cout << std::setw(2) << network_id << std::setw(10) << " HtMcs5" << std::setprecision(2)
              << std::fixed << std::setw(10) << datarate << std::setw(12) << throughput
              << std::setw(8) << totalPacketsThrough;
    if (totalPacketsThrough > 0)
    {
        std::cout << std::setw(12) << g_signalDbmAvg1 << std::setw(12) << g_noiseDbmAvg1
                  << std::setw(12) << (g_signalDbmAvg1 - g_noiseDbmAvg1) << std::endl;
    }
    else
    {
        std::cout << std::setw(12) << "N/A" << std::setw(12) << "N/A" << std::setw(12) << "N/A"
                  << std::endl;
    }
    *fileStream->GetStream() << distanceNET1toNET2 << "-" << throughput << std::endl;
}

void
initializeServerAppUDP(ApplicationContainer& serverApp,
                       NodeContainer& wifiStaNodeNet,
                       NodeContainer& wifiApNodeNet,
                       Ipv4InterfaceContainer& staNodeInterface)
{
    uint16_t port = 9;
    UdpServerHelper server(port);
    serverApp = server.Install(wifiStaNodeNet.Get(0));
    serverApp.Start(Seconds(0.0));
    serverApp.Stop(Seconds(simulationTime + 1));

    UdpClientHelper client(staNodeInterface.GetAddress(0), port);
    client.SetAttribute("MaxPackets", UintegerValue(4294967295U));
    client.SetAttribute("Interval", TimeValue(Time("0.0001"))); // packets/s
    client.SetAttribute("PacketSize", UintegerValue(payloadSize));
    ApplicationContainer clientApp = client.Install(wifiApNodeNet.Get(0));
    clientApp.Start(Seconds(1.0));
    clientApp.Stop(Seconds(simulationTime + 1));
}

void
initializeServerAppTCP(ApplicationContainer& serverApp,
                       NodeContainer& wifiStaNodeNet,
                       NodeContainer& wifiApNodeNet,
                       Ipv4InterfaceContainer& staNodeInterface)
{
    uint16_t port = 50000;
    Address localAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
    PacketSinkHelper packetSinkHelper("ns3::TcpSocketFactory", localAddress);
    serverApp = packetSinkHelper.Install(wifiStaNodeNet.Get(0));
    serverApp.Start(Seconds(0.0));
    serverApp.Stop(Seconds(simulationTime + 1));

    OnOffHelper onoff("ns3::TcpSocketFactory", Ipv4Address::GetAny());
    onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    onoff.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    onoff.SetAttribute("PacketSize", UintegerValue(payloadSize));
    onoff.SetAttribute("DataRate", DataRateValue(1000000000));
    AddressValue remoteAddress(InetSocketAddress(staNodeInterface.GetAddress(0), port));
    onoff.SetAttribute("Remote", remoteAddress);
    ApplicationContainer clientApp = onoff.Install(wifiApNodeNet.Get(0));
    clientApp.Start(Seconds(1.0));
    clientApp.Stop(Seconds(simulationTime + 1));
}

Ptr<YansWifiChannel>
createYansChannel(double frequency)
{
    YansWifiChannelHelper channel;
    channel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                               "Frequency",
                               DoubleValue(frequency * 1e6));
    channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    return channel.Create();
}

Ptr<MultiModelSpectrumChannel>
createMultiModelSpectrumChannel(double frequency)
{
    Ptr<MultiModelSpectrumChannel> multiSpectrumChannel = CreateObject<MultiModelSpectrumChannel>();
    Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
    lossModel->SetFrequency(frequency * 1e6);
    multiSpectrumChannel->AddPropagationLossModel(lossModel);

    Ptr<ConstantSpeedPropagationDelayModel> delayModel =
        CreateObject<ConstantSpeedPropagationDelayModel>();
    multiSpectrumChannel->SetPropagationDelayModel(delayModel);
    return multiSpectrumChannel;
}

double
run_script(double distanceSTAtoAP,
           double distanceNET1toNET2,
           Ptr<OutputStreamWrapper> fileStreamNet1,
           Ptr<OutputStreamWrapper> fileStreamNet2,
           networkType netType,
           bool enableSecondNetwork = true)
{
    // double distanceSTAtoAP = 1;
    // double distanceNET1toNET2 = 7;
    double txPowerStart = 1; // if 1 max distance=65
    double txPowerEnd = txPowerStart;
    std::string errorModelType = "ns3::NistErrorRateModel";
    // bool enableSecondNetwork = false;
    // typeNetwork netType = multiModel;

    std::cout << "wifiType: " << (netType == yans ? "Yans" : "MultiModel")
              << " distance: " << distanceSTAtoAP << ";" << distanceNET1toNET2
              << "m; time: " << simulationTime << "; TxPower: " << txPowerStart << " dBm (1.26 mW)"
              << std::endl;
    std::cout << std::setw(5) << "netID" << std::setw(6) << "MCS" << std::setw(13) << "Rate (Mb/s)"
              << std::setw(12) << "Tput (Mb/s)" << std::setw(10) << "Received " << std::setw(12)
              << "Signal (dBm)" << std::setw(12) << "Noi+Inf(dBm)" << std::setw(9) << "SNR (dB)"
              << std::endl;
    if (!udp)
        Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));

    NodeContainer wifiStaNodeNet1;
    wifiStaNodeNet1.Create(1);
    NodeContainer wifiApNodeNet1;
    wifiApNodeNet1.Create(1);
    NodeContainer wifiStaNodeNet2;
    wifiStaNodeNet2.Create(1);
    NodeContainer wifiApNodeNet2;
    wifiApNodeNet2.Create(1);

    YansWifiPhyHelper phy1; //  you can set only one channel-> need 2 helpers
    YansWifiPhyHelper phy2;
    SpectrumWifiPhyHelper multiSpectrumPhy;
    uint16_t frequency1 = 5180; // ch. 36 (list in /wifi/model/wifi-phy-operating-channel)
    uint16_t frequency2 = 5190; // ch. 38
    if (netType == yans)
    {
        Ptr<YansWifiChannel> channel1 = createYansChannel(frequency1);
        phy1.SetChannel(channel1);
        phy1.SetErrorRateModel(errorModelType);
        phy1.Set("TxPowerStart", DoubleValue(txPowerStart)); // dBm (1.26 mW)
        phy1.Set("TxPowerEnd", DoubleValue(txPowerEnd));
        phy1.Set("ChannelSettings", StringValue("{36, 20, BAND_5GHZ, 0}"));
        if (enableSecondNetwork)
        {
            Ptr<YansWifiChannel> channel2 = createYansChannel(frequency2);
            phy2.SetChannel(channel2);
            phy2.SetErrorRateModel(errorModelType);
            phy2.Set("TxPowerStart", DoubleValue(txPowerStart)); // dBm (1.26 mW)
            phy2.Set("TxPowerEnd", DoubleValue(txPowerEnd));
            phy2.Set("ChannelSettings", StringValue("{38, 40, BAND_5GHZ, 0}"));
        }
    }
    else if (netType == multiModel)
    {
        Ptr<MultiModelSpectrumChannel> multiSpectrumChannel1 =
            createMultiModelSpectrumChannel(frequency1);
        multiSpectrumPhy.SetChannel(multiSpectrumChannel1);
        if (enableSecondNetwork)
        {
            Ptr<MultiModelSpectrumChannel> multiSpectrumChannel2 =
                createMultiModelSpectrumChannel(frequency2);
            multiSpectrumPhy.SetChannel(multiSpectrumChannel2);
            multiSpectrumPhy.Set("ChannelSettings", StringValue("{36, 20, BAND_5GHZ, 0}"));
        }
        multiSpectrumPhy.SetErrorRateModel(errorModelType);
        multiSpectrumPhy.Set("TxPowerStart", DoubleValue(txPowerStart)); // dBm (1.26 mW)
        multiSpectrumPhy.Set("TxPowerEnd", DoubleValue(txPowerEnd));
        multiSpectrumPhy.Set("ChannelSettings", StringValue("{38, 40, BAND_5GHZ, 0}"));
    }

    WifiHelper wifi;
    wifi.SetStandard(WIFI_STANDARD_80211n);
    WifiMacHelper mac;
    WifiMacHelper mac2;

    StringValue DataRate = StringValue("HtMcs5");
    wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                 "DataMode",
                                 DataRate,
                                 "ControlMode",
                                 DataRate);

    NetDeviceContainer staDevice1;
    NetDeviceContainer apDevice1;
    NetDeviceContainer staDevice2;
    NetDeviceContainer apDevice2;

    Ssid ssid = Ssid("1ns380211n");
    Ssid ssid2 = Ssid("2ns380211n");

    if (netType == yans)
    {
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
        staDevice1 = wifi.Install(phy1, mac, wifiStaNodeNet1);
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
        apDevice1 = wifi.Install(phy1, mac, wifiApNodeNet1);
        if (enableSecondNetwork)
        {
            mac2.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2));
            staDevice2 = wifi.Install(phy2, mac2, wifiStaNodeNet2);
            mac2.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
            apDevice2 = wifi.Install(phy2, mac2, wifiApNodeNet2);
        }
    }
    else if (netType == multiModel)
    {
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
        staDevice1 = wifi.Install(multiSpectrumPhy, mac, wifiStaNodeNet1);
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
        apDevice1 = wifi.Install(multiSpectrumPhy, mac, wifiApNodeNet1);
        if (enableSecondNetwork)
        {
            mac2.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2));
            staDevice2 = wifi.Install(multiSpectrumPhy, mac2, wifiStaNodeNet2);
            mac2.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
            apDevice2 = wifi.Install(multiSpectrumPhy, mac2, wifiApNodeNet2);
        }
    }
    // Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
    //             "ShortGuardIntervalSupported",
    //             BooleanValue(false));

    // mobility.
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

    positionAlloc->Add(Vector(0.0, 0.0, 0.0));
    positionAlloc->Add(Vector(distanceSTAtoAP, 0.0, 0.0));
    if (enableSecondNetwork)
    {
        positionAlloc->Add(Vector(0.0, distanceNET1toNET2, 0.0));
        positionAlloc->Add(Vector(distanceSTAtoAP, distanceNET1toNET2, 0.0));
    }
    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

    mobility.Install(wifiApNodeNet1);
    mobility.Install(wifiStaNodeNet1);
    if (enableSecondNetwork)
    {
        mobility.Install(wifiApNodeNet2);
        mobility.Install(wifiStaNodeNet2);
    }

    /* Internet stack*/
    InternetStackHelper stack;
    stack.Install(wifiApNodeNet1);
    stack.Install(wifiStaNodeNet1);
    if (enableSecondNetwork)
    {
        stack.Install(wifiApNodeNet2);
        stack.Install(wifiStaNodeNet2);
    }
    Ipv4AddressHelper address;
    Ipv4InterfaceContainer staNodeInterface1;
    Ipv4InterfaceContainer apNodeInterface1;
    Ipv4InterfaceContainer staNodeInterface2;
    Ipv4InterfaceContainer apNodeInterface2;

    address.SetBase("192.168.1.0", "255.255.255.0");
    staNodeInterface1 = address.Assign(staDevice1);
    apNodeInterface1 = address.Assign(apDevice1);
    if (enableSecondNetwork)
    {
        address.SetBase("192.168.2.0", "255.255.255.0");
        staNodeInterface2 = address.Assign(staDevice2);
        apNodeInterface2 = address.Assign(apDevice2);
    }
    /* Setting applications */
    ApplicationContainer serverApp1;
    ApplicationContainer serverApp2;
    if (udp)
    {
        // UDP flow
        initializeServerAppUDP(serverApp1, wifiStaNodeNet1, wifiApNodeNet1, staNodeInterface1);
        if (enableSecondNetwork)
            initializeServerAppUDP(serverApp2, wifiStaNodeNet2, wifiApNodeNet2, staNodeInterface2);
    }
    else
    {
        initializeServerAppTCP(serverApp1, wifiStaNodeNet1, wifiApNodeNet1, staNodeInterface1);
        if (enableSecondNetwork)
            initializeServerAppTCP(serverApp2, wifiStaNodeNet2, wifiApNodeNet2, staNodeInterface2);
    }

    Config::ConnectWithoutContext("/NodeList/0/DeviceList/*/Phy/MonitorSnifferRx",
                                  MakeCallback(&MonitorSniffRx1));
    if (enableSecondNetwork)
        Config::ConnectWithoutContext("/NodeList/2/DeviceList/*/Phy/MonitorSnifferRx",
                                      MakeCallback(&MonitorSniffRx2));

    Simulator::Stop(Seconds(simulationTime + 1));
    SystemWallClockMs str;
    str.Start();
    // clock_t start = clock(); // CPU time
    Simulator::Run();
    // clock_t end = clock();
    int64_t el_time = str.End();
    // double elapsed = double(end - start) / CLOCKS_PER_SEC;

    retriveAndWriteData(serverApp1, 1, fileStreamNet1, distanceNET1toNET2);
    if (enableSecondNetwork)
        retriveAndWriteData(serverApp2, 2, fileStreamNet2, distanceNET1toNET2);

    // std::cout << "Clock Time measured:" << elapsed << " seconds" << std::endl;
    std::cout << "SystemClock elapsed:" << std::setprecision(2) << ((float)el_time / 1000.0) << " s"
              << std::endl;
    // std::cout << g_samples1 << std::endl;
    resetGlobalValue();
    Simulator::Destroy();
    return el_time;
}

int
main(int argc, char* argv[])
{
    // double distances[] = {90, 70, 50, 30, 20, 10, 0};
    // double distances[] = {1, 1, 1, 1, 1, 1};
    double distances[] = {10, 70, 90};
    networkType netType = multiModel;
    std::string fileNameNet1 = "throughputNet1";
    fileNameNet1.append((netType == yans ? "yans" : "multi")).append(".txt");
    std::string fileNameNet2 = "throughputNet2";
    fileNameNet2.append((netType == yans ? "yans" : "multi")).append(".txt");
    std::string fileNameTime = "texecutionTime";
    fileNameTime.append((netType == yans ? "yans" : "multi")).append(".txt");

    AsciiTraceHelper ascii_trace_helper;
    Ptr<OutputStreamWrapper> fileStream1 = ascii_trace_helper.CreateFileStream(fileNameNet1);
    Ptr<OutputStreamWrapper> fileStream2 = ascii_trace_helper.CreateFileStream(fileNameNet2);
    Ptr<OutputStreamWrapper> fileStreamTime = ascii_trace_helper.CreateFileStream(fileNameTime);
    for (int i = 0; i < 3; i++)
    {
        int time = run_script(1, distances[i], fileStream1, fileStream2, netType);
        *fileStreamTime->GetStream() << distances[i] << "-" << time << std::endl;
        std::cout << "" << std::endl;
    }
    return 0;
}