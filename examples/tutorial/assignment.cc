#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/flow-classifier.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/names.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstScript");
void double_point_to_point();
void p2p_flow();
int loss = 0;

int
main(int argc, char* argv[])
{
    Time::SetResolution(Time::NS);
    LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
    LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    double_point_to_point();
    return 0;
}

void
double_point_to_point()
{
    NodeContainer nodes_01;
    NodeContainer nodes_23;
    nodes_01.Create(2);
    nodes_23.Create(2);

    PointToPointHelper pointToPoint_01;
    pointToPoint_01.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
    pointToPoint_01.SetChannelAttribute("Delay", StringValue("2ms"));

    PointToPointHelper pointToPoint_23;
    pointToPoint_23.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
    pointToPoint_23.SetChannelAttribute("Delay", StringValue("1ms"));

    NetDeviceContainer devices_01;
    devices_01 = pointToPoint_01.Install(nodes_01);
    NetDeviceContainer devices_23;
    devices_23 = pointToPoint_23.Install(nodes_23);

    InternetStackHelper stack;
    stack.Install(nodes_01);
    stack.Install(nodes_23);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");

    Ipv4InterfaceContainer interfaces_01 = address.Assign(devices_01);
    Ipv4InterfaceContainer interfaces_23 = address.Assign(devices_23);

    UdpEchoServerHelper echoServer_01(9);
    UdpEchoServerHelper echoServer_23(10);
    double simulationTime = 10.0; // Simulation time in seconds

    ApplicationContainer serverApps_01 = echoServer_01.Install(nodes_01.Get(1));
    serverApps_01.Start(Seconds(1.0));
    serverApps_01.Stop(Seconds(simulationTime));
    ApplicationContainer serverApps_23 = echoServer_23.Install(nodes_23.Get(1));
    serverApps_23.Start(Seconds(1.0));
    serverApps_23.Stop(Seconds(simulationTime));

    UdpEchoClientHelper echoClient_01(interfaces_01.GetAddress(1), 9);
    echoClient_01.SetAttribute("MaxPackets", UintegerValue(100));
    echoClient_01.SetAttribute("Interval", TimeValue(Seconds(1)));
    echoClient_01.SetAttribute("PacketSize", UintegerValue(1024));

    UdpEchoClientHelper echoClient_23(interfaces_23.GetAddress(1), 10);
    echoClient_23.SetAttribute("MaxPackets", UintegerValue(10));
    echoClient_23.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient_23.SetAttribute("PacketSize", UintegerValue(1024));

    // pointToPoint_01.EnablePcapAll("ptp_01.pcap");
    // pointToPoint_23.EnablePcapAll("ptp_23.pcap");

    ApplicationContainer clientApps_01 = echoClient_01.Install(nodes_01.Get(0));
    clientApps_01.Start(Seconds(1.0));
    clientApps_01.Stop(Seconds(simulationTime));

    ApplicationContainer clientApps_23 = echoClient_23.Install(nodes_23.Get(0));
    clientApps_23.Start(Seconds(1.0));
    clientApps_23.Stop(Seconds(simulationTime));

    Ptr<FlowMonitor> flowMonitor;
    FlowMonitorHelper flowHelper;
    flowMonitor = flowHelper.InstallAll();

    Simulator::Stop(Seconds(simulationTime));
    Simulator::Run();

    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats();
    Ptr<Ipv4FlowClassifier> classifier =
        DynamicCast<Ipv4FlowClassifier>(flowHelper.GetClassifier());
    flowMonitor->SerializeToXmlFile("p2p_traces.xml", true, true);
    AsciiTraceHelper ascii_trace_helper;
    Ptr<OutputStreamWrapper> file_stream_throughput =
        ascii_trace_helper.CreateFileStream("simulation1_throughput.txt");
    Ptr<OutputStreamWrapper> file_stream_rtt =
        ascii_trace_helper.CreateFileStream("simulation1_rtt.txt");
    for (auto it = stats.begin(); it != stats.end(); ++it)
    {
        Ipv4FlowClassifier::FiveTuple tuple = classifier->FindFlow(it->first);
        double rtt =
            (it->second.delaySum.GetSeconds() / it->second.rxPackets) * 1000; // RTT in milliseconds
        double throughput = it->second.txBytes * 8 / simulationTime / (1 << 10); // Kbps
        std::cout << "Flow: " << it->first << std::endl;
        std::cout << "Source IP: " << tuple.sourceAddress << " Port: " << tuple.sourcePort
                  << std::endl;
        std::cout << "Destination IP: " << tuple.destinationAddress
                  << " Port: " << tuple.destinationPort << std::endl;
        std::cout << "RTT: " << rtt << " ms" << std::endl;
        std::cout << "Throughput per second: " << throughput << " Kbps" << std::endl;
        std::cout << std::endl;
        *file_stream_rtt->GetStream() << it->first << "," << rtt << std::endl;
        *file_stream_throughput->GetStream() << it->first << "," << throughput << std::endl;
    }
    Simulator::Destroy();
}